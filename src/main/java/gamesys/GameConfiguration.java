package gamesys;

import gamesys.io.InputReader;
import gamesys.io.OutputWriter;

import java.util.List;

/**
 * All things needed for the game to begin.
 * Created by Julia Fernee on 22/06/2015.
 */
public interface GameConfiguration {
    List<String> getPlayers();

    InputReader getReader();

    OutputWriter getWriter();
}

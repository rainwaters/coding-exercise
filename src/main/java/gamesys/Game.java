package gamesys;

import java.util.Map;

/**
 * Game lifecycle operations.
 * Created by Julia Fernee on 22/06/2015.
 */
public interface Game {
    void start(GameConfiguration gameConfiguration);

    boolean playRound();

    Map<String, Double[]> processTotals();

    void terminate();

    boolean isTerminated();

    void end();
}

package gamesys;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import gamesys.service.bet.AsyncBetProcessor;
import gamesys.service.numgen.RandomNumberGenerator;
import gamesys.service.numgen.RandomNumberGeneratorService;
import gamesys.service.numgen.ScheduledRandomNumberGenerator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Jlia Fernee on 22/06/2015.
 */
@EnableConfigurationProperties
@Configuration
@PropertySource("application.properties")
public class EventBusConfiguration {

    private Logger log = Logger.getLogger(EventBusConfiguration.class);

    private ExecutorService executorService;
    private CountDownLatch countDownLatch;
    private EventBus eventBus;
    private RandomNumberGeneratorService randomNumberGeneratorService;
    private AsyncBetProcessor asyncBetProcessor;

    @Value("${numberGenerator.Rate.Milliseconds}")
    private long numberGeneratorRateMs;
    @Value("${numberGenerator.Range.From.Inclusive}")
    private int numberGeneratorRangeFromInclusive;
    @Value("${numberGenerator.Range.To.Inclusive}")
    private int numberGeneratorRangeToInclusive;
    @Value("${betRequest.Timeout.Milliseconds}")
    private long betRequestTimeoutMs;

    @PostConstruct
    public void init() {

        executorService = Executors.newCachedThreadPool();
        countDownLatch = new CountDownLatch(1);
        eventBus = new AsyncEventBus(executorService);
        createRandomNumberGeneratorService();
        asyncBetProcessor = new AsyncBetProcessor(eventBus, countDownLatch,
                betRequestTimeoutMs);
        eventBus.register(randomNumberGeneratorService);
        log.info("Event listerners are registered with event bus");
    }

    @Bean
    public EventBus eventBus() {
        log.info("GameEventBus is registered with spring context");
        return eventBus;
    }

    @Bean
    public AsyncBetProcessor asyncBetProcessor() {
        log.info("AsyncBetProcessor is registered with spring context");
        return asyncBetProcessor;
    }

    @Bean
    public RandomNumberGeneratorService randomNumberGeneratorService() {
        log.info("RandomNumberGeneratorService is registered with spring context");
        return randomNumberGeneratorService;
    }


    private RandomNumberGenerator createRandomNumberGenerator() {
        return new ScheduledRandomNumberGenerator(
                numberGeneratorRateMs,
                numberGeneratorRangeFromInclusive,
                numberGeneratorRangeToInclusive);
    }

    private void createRandomNumberGeneratorService() {
        randomNumberGeneratorService = new RandomNumberGeneratorService();
        randomNumberGeneratorService.setCountDownLatch(countDownLatch);
        randomNumberGeneratorService.setRandomNumberGenerator(createRandomNumberGenerator());
        executorService.submit(randomNumberGeneratorService);
    }

    @PreDestroy
    public void destroy() {
        eventBus.unregister(randomNumberGeneratorService);
        log.info("Unregister listerners from event bus");
        executorService.shutdown();
        log.info("Sutdown thread pool");
    }

}

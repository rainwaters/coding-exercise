package gamesys.service.numgen;

/**
 * Created by Julia Fernee on 20/06/2015.
 */
public interface RandomNumberGenerator {

    int getRandomNumber();

    void start();

    void stop();
}

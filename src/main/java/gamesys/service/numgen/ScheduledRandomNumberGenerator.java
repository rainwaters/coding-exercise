package gamesys.service.numgen;

import org.apache.log4j.Logger;

import java.util.PrimitiveIterator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

/**
 * Created by Julia Fernee on 20/06/2015.
 */
public class ScheduledRandomNumberGenerator implements RandomNumberGenerator {
    private Logger log = Logger.getLogger(this.getClass());

    private final Long interval;
    private Long updateTime;
    int fromRangeInclusive;
    int toRangeInclusive;
    private IntStream stream;
    private PrimitiveIterator.OfInt iter;
    private volatile int randNum;

    public ScheduledRandomNumberGenerator(Long interval, int fromRangeInclusive, int toRangeInclusive) {
        this.interval = interval;
        this.fromRangeInclusive = fromRangeInclusive;
        this.toRangeInclusive = toRangeInclusive;
    }

    public void start() {
        stream = ThreadLocalRandom.current().ints(fromRangeInclusive, toRangeInclusive + 1);
        iter = stream.iterator();
        randNum = iter.next();
        updateTime = System.currentTimeMillis() + interval;
        log.info("started");
    }

    public int getRandomNumber() {
        if (System.currentTimeMillis() >= updateTime) {
            randNum = iter.next();
            updateTime = System.currentTimeMillis() + interval;
        }
        return randNum;
    }

    public void stop() {
        stream.close();
    }
}

package gamesys.service.numgen;

import com.google.common.eventbus.Subscribe;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
@Component
public class RandomNumberGeneratorService implements Runnable {
    private Logger log = Logger.getLogger(RandomNumberGeneratorService.class);

    private AtomicBoolean stop;
    private AtomicInteger randomNumber;
    private CountDownLatch countDownLatch;
    private RandomNumberGenerator randomNumberGenerator;

    public RandomNumberGeneratorService() {
        this.stop = new AtomicBoolean(false);
        this.randomNumber = new AtomicInteger();
    }

    public void run() {

        randomNumberGenerator.start();
        randomNumber.set(randomNumberGenerator.getRandomNumber());
        log.info("Started with first number " + randomNumber.get());
        if (countDownLatch != null) {
            countDownLatch.countDown();
        }

        while (!stop.get()) {
            randomNumber.set(randomNumberGenerator.getRandomNumber());
        }
    }

    @Subscribe
    public void onEvent(RandomNumberRequest r) {
        r.setResponse(getNextRandom());
    }

    @Subscribe
    public void onEvent(StopRandomNumberGeneratorRequest e) {
        stop();
    }

    private int getNextRandom() {
        return randomNumber.get();
    }

    private void stop() {
        stop.set(true);
        randomNumberGenerator.stop();
    }

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public void setRandomNumberGenerator(RandomNumberGenerator randomNumberGenerator) {
        this.randomNumberGenerator = randomNumberGenerator;
    }
}

package gamesys.service.numgen;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class RandomNumberRequest implements Serializable {

    private transient Logger log = Logger.getLogger(this.getClass());
    private Integer number;

    private CountDownLatch latch = new CountDownLatch(1);

    public Integer getResponse(long timeoutMS) {
        try {
            latch.await(timeoutMS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            log.warn("Thread was interrupted before receiving RandomNumberResponse.");
        }
        return number;
    }

    public void setResponse(int number) {
        this.number = number;
        latch.countDown();
    }
}

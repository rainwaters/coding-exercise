package gamesys.service.bet;

import com.google.common.eventbus.EventBus;
import gamesys.service.numgen.RandomNumberRequest;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class AsyncBetProcessor {
    private Logger log = Logger.getLogger(this.getClass());

    private EventBus gameEventBus;
    private CountDownLatch latch;
    private long betRequestTimeoutMs;

    public AsyncBetProcessor(EventBus gameEventBus, CountDownLatch latch, long betRequestTimeoutMs) {
        this.gameEventBus = gameEventBus;
        this.latch = latch;
        this.betRequestTimeoutMs = betRequestTimeoutMs;
    }

    //producer
    public void submit(List<BetRequest> bets) {
        //precaution: should not start posting to the service that has not yet been initialised
        try {
            latch.await();
        } catch (InterruptedException e) {
            log.warn(this.getClass().getSimpleName() + "was interrupted");
        }
        RandomNumberRequest numberRequest = new RandomNumberRequest();
        gameEventBus.post(numberRequest);
        //may throw runtime TimeoutException.
        // Make sure the timeout property is set to the interval sufficient for your machine
        Integer number = numberRequest.getResponse(betRequestTimeoutMs);
        for (BetRequest betRequest : bets) {
            BetResponse betResponse = betRequest.getBetStrategy().play(betRequest, number);
            betRequest.setBetResponse(betResponse);
        }
    }
}

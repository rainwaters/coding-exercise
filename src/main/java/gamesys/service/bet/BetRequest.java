package gamesys.service.bet;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class BetRequest implements Serializable {
    private transient Logger log = Logger.getLogger(this.getClass());

    private String playerName;
    private double betAmount;
    private BetStrategy betStrategy;
    private BetResponse betResponse;
    private transient CountDownLatch latch;

    public BetRequest(String playerName, double betAmount, BetStrategy betStrategy) {
        this.playerName = playerName;
        this.betAmount = betAmount;
        this.betStrategy = betStrategy;
        latch = new CountDownLatch(1);
    }

    public String getPlayerName() {
        return playerName;
    }

    public double getBetAmount() {
        return betAmount;
    }

    public BetStrategy getBetStrategy() {
        return betStrategy;
    }

    public BetResponse getBetResponse() {
        try {
            latch.await();
        } catch (InterruptedException e) {
            log.warn("Thread was interrupted before receiving BetResponse.");
        }
        return betResponse;
    }

    public void setBetResponse(BetResponse betResponse) {
        latch.countDown();
        this.betResponse = betResponse;
    }

    @Override
    public String toString() {
        return "BetRequest{" +
                "playerName='" + playerName + '\'' +
                ", betAmount=" + betAmount +
                ", betStrategy=" + betStrategy +
                ", betResponse=" + betResponse +
                '}';
    }
}

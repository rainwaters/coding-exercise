package gamesys.service.bet;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class OddEvenStrategy implements BetStrategy, Serializable {
    private transient Logger log = Logger.getLogger(this.getClass());
    public static final int WIN_FACTOR = 2;

    public static enum BET {
        ODD(1), EVEN(0);
        private int value;

        BET(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    ;

    private BET bet;

    public OddEvenStrategy(BET bet) {
        this.bet = bet;
    }

    @Override
    public BetResponse play(BetRequest betRequest, Integer luckyNumber) {
        if (luckyNumber % 2 == bet.getValue()) {
            double winAmount = betRequest.getBetAmount() * WIN_FACTOR;
            return new BetResponse(winAmount, "WIN", luckyNumber);
        }
        return new BetResponse(0, "LOOSE", luckyNumber);
    }

    @Override
    public String getBet() {
        return bet.name();
    }


    @Override
    public String toString() {
        return "OddEvenStrategy{" +
                "bet='" + getBet() +
                "'}";
    }
}

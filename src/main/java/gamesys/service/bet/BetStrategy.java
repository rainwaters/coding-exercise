package gamesys.service.bet;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public interface BetStrategy {

    BetResponse play(BetRequest betRequest, Integer luckyNumber);

    String getBet();
}

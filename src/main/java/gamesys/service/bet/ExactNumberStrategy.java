package gamesys.service.bet;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class ExactNumberStrategy implements BetStrategy, Serializable {
    private transient Logger log = Logger.getLogger(this.getClass());

    public static final int WIN_FACTOR = 36;

    private int betNumber;

    public ExactNumberStrategy(int betNumber) {
        this.betNumber = betNumber;
    }

    @Override
    public BetResponse play(BetRequest betRequest, Integer luckyNumber) {
        double winAmount = 0;
        if (betNumber == luckyNumber) {
            winAmount = betRequest.getBetAmount() * WIN_FACTOR;
            return new BetResponse(winAmount, "WIN", luckyNumber);
        }
        return new BetResponse(winAmount, "LOOSE", luckyNumber);
    }

    @Override
    public String getBet() {
        return String.valueOf(betNumber);
    }

    @Override
    public String toString() {
        return "ExactNumberStrategy{" +
                "bet='" + getBet() +
                "'}";
    }
}

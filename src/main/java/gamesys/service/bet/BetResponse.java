package gamesys.service.bet;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class BetResponse implements Serializable {
    private transient Logger log = Logger.getLogger(this.getClass());

    private double winAmount;
    private String outcome;
    private int luckyNumber;

    public BetResponse(double winAmount, String outcome, int luckyNumber) {
        this.winAmount = winAmount;
        this.outcome = outcome;
        this.luckyNumber = luckyNumber;
    }

    public double getWinAmount() {
        return winAmount;
    }

    public String getOutcome() {
        return outcome;
    }

    public int getLuckyNumber() {
        return luckyNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BetResponse that = (BetResponse) o;

        if (Double.compare(that.winAmount, winAmount) != 0) return false;
        if (luckyNumber != that.luckyNumber) return false;
        return !(outcome != null ? !outcome.equals(that.outcome) : that.outcome != null);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(winAmount);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (outcome != null ? outcome.hashCode() : 0);
        result = 31 * result + luckyNumber;
        return result;
    }

    @Override
    public String toString() {
        return "BetResponse{" +
                "winAmount=" + winAmount +
                ", outcome='" + outcome + '\'' +
                ", luckyNumber=" + luckyNumber +
                '}';
    }
}

package gamesys.service.bet;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class BetRequestValidationException extends Exception {

    private static String message = "Did not recognise your betting instruction";

    public BetRequestValidationException() {
        super(message);
    }

    @Override
    public String toString() {
        return this.getMessage();
    }
}

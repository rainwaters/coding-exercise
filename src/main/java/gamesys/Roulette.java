package gamesys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring boot application entry point.
 * Created by Julia Fernee on 22/06/2015.
 */
@ComponentScan
@EnableAutoConfiguration
public class Roulette {
    public static void main(String[] args) {
        SpringApplication.run(Roulette.class, args);
    }
}

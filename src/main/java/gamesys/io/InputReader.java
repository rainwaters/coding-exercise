package gamesys.io;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public interface InputReader {
    String readBetRequest();

    void shutdown();
}

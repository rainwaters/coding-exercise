package gamesys.io;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public interface OutputWriter {
    String GAME_OVER = "done";

    void write(String... words);

    void writeLine(String... lines);
}

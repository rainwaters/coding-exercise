package gamesys.console;

import gamesys.Game;
import gamesys.GameConfiguration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
@Controller
public class ConsoleGameController implements CommandLineRunner {
    private Logger log = Logger.getLogger(this.getClass());

    @Autowired
    @Qualifier("consoleGame")
    private Game consoleGame;


    private GameConfiguration gameConfiguration;

    public ConsoleGameController() {
        gameConfiguration = new ConsoleGameConfiguration(new ConsoleReader(), new ConsoleWriter());
    }

    public void run(String... args) throws Exception {
        //load players from file
        //start game round
        //for each player
        //read player's bet input
        // Create betRequest
        //submit to service
        //end game round
        //write outcome (wins/looses) for each player
        //until "game over" input
        //write totals to console


        consoleGame.start(gameConfiguration);
        while (consoleGame.playRound()) {
            ;
        }
        consoleGame.processTotals();
        consoleGame.end();
        System.exit(0);
    }
}

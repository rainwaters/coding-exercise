package gamesys.console;

import gamesys.GameConfiguration;
import gamesys.io.InputReader;
import gamesys.io.OutputWriter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import static java.nio.file.Paths.get;

/**
 * Created by Julia Fernee on 22/06/2015.
 */

public class ConsoleGameConfiguration implements GameConfiguration {
    private Logger log = Logger.getLogger(this.getClass());
    private List<String> players;

    private InputReader reader;

    private OutputWriter writer;

    public ConsoleGameConfiguration(InputReader reader, OutputWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    @Override
    public List<String> getPlayers() {
        if (players == null) {
            readPlayersFromFile();
        }
        return players;
    }

    public InputReader getReader() {
        return reader;
    }

    public OutputWriter getWriter() {
        return writer;
    }

    private void readPlayersFromFile() {
        //want to preserve input order it will be used to output scores
        players = new LinkedList<>();
        Path path = null;
        try {
            path = get(getClass().getResource("/players.txt").toURI());
            Files.lines(path).forEach(l -> players.add(l));

        } catch (URISyntaxException e) {
            ; //never happens
        } catch (IOException e) {
            //will fail game initialisation and the game will not start
            throw new RuntimeException(e);
        }
    }
}

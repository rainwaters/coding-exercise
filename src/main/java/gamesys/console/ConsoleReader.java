package gamesys.console;

import gamesys.io.InputReader;

import java.util.Scanner;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class ConsoleReader implements InputReader {

    private Scanner scanner;

    public ConsoleReader() {
        scanner = new Scanner(System.in);
    }

    @Override
    public String readBetRequest() {
        String instruction = scanner.nextLine();
        return instruction;
    }

    @Override
    public void shutdown() {
        scanner.close();
    }


}

package gamesys.console;

import gamesys.io.OutputWriter;

/**
 * Created by Julia Fernee on 22/06/2015.
 */

public class ConsoleWriter implements OutputWriter {

    @Override
    public void write(String... words) {
        for (String word : words) {
            System.out.print(word + " ");
        }
    }

    @Override
    public void writeLine(String... lines) {
        for (String line : lines) {
            System.out.println(line);
        }
    }

}

package gamesys.console;

import gamesys.Game;
import gamesys.GameConfiguration;
import gamesys.service.bet.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
@Component("consoleGame")
public class ConsoleGame implements Game {

    private Logger log = Logger.getLogger(this.getClass());
    public static final String END_GAME = "done";
    private boolean terminated;

    private Map<Integer, List<BetRequest>> scores;
    private GameConfiguration gameConfiguration;

    @Autowired
    private AsyncBetProcessor asyncBetProcessor;

    @Override
    public void start(GameConfiguration gameConfiguration) {
        this.gameConfiguration = gameConfiguration;
        scores = new ConcurrentHashMap<>();
        gameConfiguration.getWriter().
                writeLine("\nPlayers play rounds until one player types \"done\"");
        gameConfiguration.getWriter().
                writeLine("The round when one player types \"done\" is not counted\n");
    }

    @Override
    public void terminate() {
        this.terminated = true;
    }

    @Override
    public boolean isTerminated() {
        return terminated;
    }

    @Override
    public boolean playRound() {

        gameConfiguration.getWriter().writeLine("\nNew Round");

        List<String> players = gameConfiguration.getPlayers();
        //doesn't need to be thread safe - only used by this thread
        //however elements in the list are modified by another thread
        List<BetRequest> bets = new LinkedList<>();
        try {
            for (String player : players) {
                gameConfiguration.getWriter().write(player);
                BetRequest betRequest = readBetRequest(player, gameConfiguration.getReader().readBetRequest());
                //any player can terminate the game
                // in that case the round during which the game was terminated is not counted
                //even if some but not all players have already played
                if (isTerminated()) {
                    return false;
                }
                //eventBus.post(betRequest);
                bets.add(betRequest);

            }
            asyncBetProcessor.submit(bets);
            scores.put(scores.size(), bets);
            displayScores();
        } catch (BetRequestValidationException e) {
            gameConfiguration.getWriter().writeLine(e.toString());
        } catch (Exception e) {
            gameConfiguration.getWriter().writeLine("This round will not be counted because of technical problems");
            gameConfiguration.getWriter().writeLine(e.getMessage());
        }
        return true;
    }

    @Override
    public Map<String, Double[]> processTotals() {
        Collection<List<BetRequest>> c = scores.values();
        c.stream().forEach(s -> {
        });
        Map<String, Double[]> totals = initialiseTotals();
        for (List<BetRequest> round : c) {
            round.forEach(r -> {
                double spent = r.getBetAmount();
                double win = r.getBetResponse().getWinAmount();
                Double[] score = totals.get(r.getPlayerName());
                score[0] += +spent;
                score[1] += win;
            });
        }
        gameConfiguration.getWriter().writeLine(String.format("%-20s %15s %15s", "Player", "Total Win", "Total Bet"));
        gameConfiguration.getWriter().writeLine("---");
        for (String player : gameConfiguration.getPlayers()) {
            gameConfiguration.getWriter().writeLine(String.format("%-20s %15.1f %15.1f ", player, totals.get(player)[1], totals.get(player)[0]));
        }
        return totals;
    }

    @Override
    public void end() {
        gameConfiguration.getReader().shutdown();
        gameConfiguration.getWriter().writeLine("\nGame Over");
    }

    private void displayScores() {
        List<BetRequest> lastRound = scores.get(scores.size() - 1);
        int number = lastRound.get(0).getBetResponse().getLuckyNumber();

        gameConfiguration.getWriter().writeLine(String.format("Number: %5d", number));
        gameConfiguration.getWriter().writeLine(String.format("%-20s %15s %15s %15s", "Player", "Bet", "Outcome", "Winnings"));
        gameConfiguration.getWriter().writeLine("---");
        for (BetRequest player : lastRound) {
            String out = String.format("%-20s %15s %15s %15.1f", player.getPlayerName(), player.getBetStrategy().getBet(),
                    player.getBetResponse().getOutcome(), player.getBetResponse().getWinAmount());
            gameConfiguration.getWriter().writeLine(out);
        }
    }

    private Map<String, Double[]> initialiseTotals() {
        Map<String, Double[]> totals = new HashMap<>();
        List<String> players = gameConfiguration.getPlayers();
        for (String player : players) {
            totals.put(player, new Double[]{0.0, 0.0});
        }
        return totals;
    }

    private BetRequest readBetRequest(String player, String instruction) throws BetRequestValidationException {
        Double stake = null;
        if (endGameDetected(instruction)) {
            return null;
        }

        String[] parts = instruction.split(" ");
        if (parts.length != 2) {
            throw new BetRequestValidationException();
        }
        try {
            stake = Double.parseDouble(parts[1].trim());
            Double zero = new Double(0);
            if (stake.equals(zero) || stake < 0) {
                throw new BetRequestValidationException();
            }
        } catch (NumberFormatException e) {
            throw new BetRequestValidationException();
        }

        BetStrategy strategy = resolveBetStrategy(parts[0]);
        BetRequest request = new BetRequest(player, stake, strategy);
        return request;
    }

    private boolean endGameDetected(String betInstruction) {
        String instruction = betInstruction.trim().toLowerCase(Locale.getDefault());
        if (instruction.equals(ConsoleGame.END_GAME)) {
            terminate();
            return true;
        }
        return false;
    }

    private BetStrategy resolveBetStrategy(String betValue) throws BetRequestValidationException {
        String bet = betValue.trim().toUpperCase(Locale.getDefault());

        if (bet.equals(OddEvenStrategy.BET.EVEN.name())) {
            return new OddEvenStrategy(OddEvenStrategy.BET.EVEN);
        } else if (bet.equals(OddEvenStrategy.BET.ODD.name())) {
            return new OddEvenStrategy(OddEvenStrategy.BET.ODD);
        } else {
            try {
                int betNumber = Integer.parseInt(betValue);
                return new ExactNumberStrategy(betNumber);

            } catch (NumberFormatException e) {
                throw new BetRequestValidationException();
            }
        }
    }
}

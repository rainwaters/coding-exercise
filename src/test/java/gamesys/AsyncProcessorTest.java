package gamesys;

import gamesys.service.bet.AsyncBetProcessor;
import gamesys.service.bet.BetRequest;
import gamesys.service.bet.ExactNumberStrategy;
import gamesys.service.bet.OddEvenStrategy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Julia Fernee on 24/06/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class AsyncProcessorTest {

    @Autowired
    AsyncBetProcessor asyncBetProcessor;

    @Test
    public void contextInjected() {
        Assert.assertTrue(asyncBetProcessor != null);
    }

    @Test
    public void shouldGtScoreForOneBetRequest() {

        List<BetRequest> bets = new LinkedList<>();
        bets.add(new BetRequest("Test", 1.0, new OddEvenStrategy(OddEvenStrategy.BET.EVEN)));

        asyncBetProcessor.submit(bets);

        for (BetRequest bet : bets) {
            Assert.assertNotNull(bet.getBetResponse());
        }
    }

    @Test
    public void shouldGtScoreForTreeBetRequests() {

        List<BetRequest> bets = new LinkedList<>();
        bets.add(new BetRequest("Test1", 1.0, new OddEvenStrategy(OddEvenStrategy.BET.EVEN)));
        bets.add(new BetRequest("Test2", 1.0, new OddEvenStrategy(OddEvenStrategy.BET.ODD)));
        bets.add(new BetRequest("Test3", 1.0, new ExactNumberStrategy(1)));

        asyncBetProcessor.submit(bets);

        for (BetRequest bet : bets) {
            Assert.assertNotNull(bet.getBetResponse());
        }
    }

}

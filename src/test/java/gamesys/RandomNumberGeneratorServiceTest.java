package gamesys;

import gamesys.service.numgen.RandomNumberGenerator;
import gamesys.service.numgen.RandomNumberGeneratorService;
import gamesys.service.numgen.ScheduledRandomNumberGenerator;
import gamesys.service.numgen.StopRandomNumberGeneratorRequest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.*;

/**
 * Created by Julia Fernee on 20/06/2015.
 */
public class RandomNumberGeneratorServiceTest {

    /**
     * Invariants for all tests
     */
    private static long numberGenerationRatelMs = 10;
    private static int fromRangeInclusive = 1;
    private static int toRangeInclusive = 36;
    private static final long callbackTimeout = 30000;


    private CountDownLatch latch;
    private RandomNumberGeneratorService numGenService;
    private ExecutorService executorService;


    @Before
    public void setUp() {
        latch = new CountDownLatch(1);
        numGenService = new RandomNumberGeneratorService();
        RandomNumberGenerator randomNumberGenerator = new ScheduledRandomNumberGenerator(numberGenerationRatelMs, fromRangeInclusive, toRangeInclusive);
        numGenService.setRandomNumberGenerator(randomNumberGenerator);
        numGenService.setCountDownLatch(latch);
        executorService = Executors.newCachedThreadPool();
    }

    @After
    public void cleanUp() {
        executorService.shutdown();
        System.out.println("Shutdown thread pool");
        numGenService.onEvent(new StopRandomNumberGeneratorRequest());
        System.out.println("Stopped RandomNumberGeneratorService");
    }

    @Test
    public void shouldProvideNumbersToOneConsumer() throws InterruptedException, ExecutionException, TimeoutException {
        try {
            executorService.submit(numGenService);
            int nRuns = 10;
            Future<List<TestConsumer.TestData>> callback = runNumGenServiceConsumer("oneConsumer", 100, nRuns, false);
            List<TestConsumer.TestData> actualNumbers = callback.get(callbackTimeout, TimeUnit.MILLISECONDS);
            Assert.assertEquals("Unexpected number of results.", nRuns, actualNumbers.size());
        } finally {
            ; //After test cleanUp will do the job
        }
    }

    @Test
    public void shouldProvideNumbersWithinRangeToOneConsumer() throws InterruptedException, ExecutionException, TimeoutException {
        try {
            executorService.submit(numGenService);
            Future<List<TestConsumer.TestData>> callback = runNumGenServiceConsumer("inRange", 10, 1000, false);
            List<TestConsumer.TestData> actualNumbers = callback.get(callbackTimeout, TimeUnit.MILLISECONDS);
            Assert.assertTrue("Not all numbers are within a specified range", allInRange(actualNumbers));
        } finally {
            ; //After test cleanUp will do the job
        }
    }

    @Test
    public void shouldProvideNumbersToTwoConsumers() throws InterruptedException, ExecutionException, TimeoutException {

        try {
            executorService.submit(numGenService);
            int nRuns = 5;
            long delay = 30000;
            Future<List<TestConsumer.TestData>> callback1 = runNumGenServiceConsumer("c1", 100, nRuns, false);
            Future<List<TestConsumer.TestData>> callback2 = runNumGenServiceConsumer("c2", 110, nRuns, false);

            List<TestConsumer.TestData> actualNumbers1 = callback1.get(delay, TimeUnit.MILLISECONDS);
            List<TestConsumer.TestData> actualNumbers2 = callback2.get(delay, TimeUnit.MILLISECONDS);
            Assert.assertTrue(actualNumbers1.size() == nRuns && actualNumbers2.size() == nRuns);
        } finally {
            ; //After test cleanUp will do the job
        }
    }

    private boolean allInRange(List<TestConsumer.TestData> testData) {
        boolean match = testData.stream().map(td -> td.getNumber()).anyMatch(n -> {
            return (n > toRangeInclusive || n < fromRangeInclusive);
        });
        return !match;
    }

    private Future<List<TestConsumer.TestData>> runNumGenServiceConsumer(String id, int interval, int nRuns, boolean withLog) throws InterruptedException, ExecutionException, TimeoutException {
        TestConsumer c = (new TestConsumer(numGenService, interval, nRuns, id)).withCountDownLatch(latch).withLog(withLog);
        Future<List<TestConsumer.TestData>> cRuns = executorService.submit(c);
        return cRuns;
    }

    private void printTestData(List<TestConsumer.TestData> results) {
        System.out.println("Printing test data ....");
        results.stream().forEach(c -> {
            c.print();
        });
    }
}

package gamesys;

import gamesys.console.ConsoleGameConfiguration;
import gamesys.io.InputReader;
import gamesys.io.OutputWriter;
import gamesys.service.bet.AsyncBetProcessor;
import gamesys.service.bet.BetRequest;
import gamesys.service.bet.ExactNumberStrategy;
import gamesys.service.bet.OddEvenStrategy;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class GameRoundTest {

    @Autowired
    @Qualifier("testGame")
    private Game testGameSpy;

    @Autowired
    AsyncBetProcessor asyncBetProcessor;

    @Mock
    private InputReader reader;
    @Mock
    private OutputWriter writer;
    @Captor
    private ArgumentCaptor<List<BetRequest>> betRequestCaptor;

    private GameConfiguration conf;

    @Before
    public void configureGame() {
        MockitoAnnotations.initMocks(this);
        conf = new ConsoleGameConfiguration(reader, writer);
        testGameSpy.start((conf));
    }

    @Test
    public void contextInjected() {

        Assert.assertTrue(testGameSpy != null);
        Assert.assertTrue(asyncBetProcessor != null);
    }

    @Test
    public void shouldGetBetResponsesWhenSubmitsBetsToBetRocessor() {

        when(reader.readBetRequest()).thenReturn("ODD 1.0");
        testGameSpy.playRound();
        verify(asyncBetProcessor, atLeastOnce()).submit(betRequestCaptor.capture());

        Assert.assertNotNull(betRequestCaptor.getValue().get(0).getBetResponse());
        Assert.assertNotNull(betRequestCaptor.getValue().get(1).getBetResponse());
    }

    @Test
    public void shouldResolveOddEvenBetStrategy() {
        when(reader.readBetRequest()).thenReturn("EVEN 1.0");
        testGameSpy.playRound();

        verify(asyncBetProcessor, atLeastOnce()).submit(betRequestCaptor.capture());
        BetRequest rec = betRequestCaptor.getValue().get(0);
        Assert.assertEquals(OddEvenStrategy.class, rec.getBetStrategy().getClass());
    }

    @Test
    public void shouldResolveExactNumberStrategy() {
        when(reader.readBetRequest()).thenReturn("1 1.0");
        testGameSpy.playRound();

        verify(asyncBetProcessor, atLeastOnce()).submit(betRequestCaptor.capture());
        BetRequest rec = betRequestCaptor.getValue().get(0);
        Assert.assertEquals(ExactNumberStrategy.class, rec.getBetStrategy().getClass());
    }

    @Test
    public void shouldSetBetAmount() {
        when(reader.readBetRequest()).thenReturn("1 1.0");
        testGameSpy.playRound();

        verify(asyncBetProcessor, atLeastOnce()).submit(betRequestCaptor.capture());
        double actualAmount = betRequestCaptor.getValue().get(0).getBetAmount();
        Assert.assertThat(actualAmount, CoreMatchers.equalTo(1.0));

    }

    @Test
    public void shouldSetPlayersNamesInOrder() {
        when(reader.readBetRequest()).thenReturn("1 1.0");
        testGameSpy.playRound();

        verify(asyncBetProcessor, atLeastOnce()).submit(betRequestCaptor.capture());
        String tiki_monkey = betRequestCaptor.getValue().get(0).getPlayerName();
        String barbara = betRequestCaptor.getValue().get(1).getPlayerName();
        Assert.assertThat(tiki_monkey, CoreMatchers.equalTo("Tiki_Monkey"));
        Assert.assertThat(barbara, CoreMatchers.equalTo("Barbara"));
    }
}

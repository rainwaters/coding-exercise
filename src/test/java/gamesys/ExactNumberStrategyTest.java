package gamesys;

import gamesys.service.bet.BetRequest;
import gamesys.service.bet.BetResponse;
import gamesys.service.bet.BetStrategy;
import gamesys.service.bet.ExactNumberStrategy;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Julia Fernee on 21/06/2015.
 */
public class ExactNumberStrategyTest {

    /**
     * Given bet request of bet amount 1.1 and bet number 1
     * when lucky number matches
     * than bet outcome is "WIN" and the winning amount is 1.1 x 36
     */
    @Test
    public void shouldFactorWinAmountForWinningBet() {
        int luckyNumber = 1;
        BetStrategy strategy = new ExactNumberStrategy(luckyNumber);
        BetRequest request = new BetRequest("Barbara", 1.1, strategy);
        BetResponse expectedBetResponse = new BetResponse(39.6, "WIN", luckyNumber);

        BetResponse actualResponse = strategy.play(request, luckyNumber);

        Assert.assertEquals("Incorrect bet response", expectedBetResponse, actualResponse);

    }

    /**
     * Given bet request of bet amount 1.1 and bet number 1
     * when lucky number does not match
     * than bet outcome is "LOOSE" and the winning amount is 0
     */
    @Test
    public void shouldZeroWinAmountForLoosingBet() {
        int luckyNumber = 36;
        int betNumber = 1;
        BetStrategy strategy = new ExactNumberStrategy(betNumber);
        BetRequest request = new BetRequest("Tikki_Monkey", 1.1, strategy);
        BetResponse expectedBetResponse = new BetResponse(0, "LOOSE", luckyNumber);

        BetResponse actualResponse = strategy.play(request, luckyNumber);

        Assert.assertEquals("Incorrect bet response", expectedBetResponse, actualResponse);

    }
}

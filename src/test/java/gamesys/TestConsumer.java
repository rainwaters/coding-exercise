package gamesys;

import gamesys.service.numgen.RandomNumberGeneratorService;
import gamesys.service.numgen.RandomNumberRequest;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class TestConsumer implements Callable<List<TestConsumer.TestData>> {

    private final RandomNumberGeneratorService service;
    private final int interval;
    private final String id;
    private List<TestConsumer.TestData> runs;
    int nRuns;

    private CountDownLatch countDownLatch;
    private boolean log;

    public TestConsumer withCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
        return this;
    }

    public TestConsumer withLog(boolean log) {
        this.log = log;
        return this;
    }

    public TestConsumer(RandomNumberGeneratorService sub, int interval, int nRuns, String id) {
        this.service = sub;
        this.interval = interval;
        this.nRuns = nRuns;
        this.id = id;
        runs = new LinkedList();
    }

    private void consume() {
        long currentRun = System.currentTimeMillis();
        long nextRun = currentRun + interval;

        int count = nRuns;
        System.out.println();
        while (count > 0) {
            currentRun = System.currentTimeMillis();
            if (currentRun > nextRun) {
                count--;
                nextRun += interval;
                RandomNumberRequest r = new RandomNumberRequest();
                service.onEvent(r);
                int number = r.getResponse(interval);
                TestData td = new TestData(id, number, currentRun, nextRun);
                runs.add(td);

                if (log) {
                    td.print();
                }
            }
        }
    }

    public List<TestConsumer.TestData> call() throws Exception {
        try {
            countDownLatch.await();
            consume();
        } catch (InterruptedException e) {
            Thread.currentThread().isInterrupted();
            System.out.println(String.format("TestConsumer %s is interrupted.", id));
        }
        return runs;
    }

    public class TestData {
        private final DateTimeFormatter fmt = DateTimeFormat.forPattern("ss:SSS");
        private int number;
        private long currentRun;
        long nextRun;
        private String id;

        public TestData(String id, int number, long currentTime, long nextRun) {
            this.id = id;
            this.number = number;
            this.currentRun = currentTime;
            this.nextRun = nextRun;
        }

        public int getNumber() {
            return number;
        }

        public long getCurrentRun() {
            return currentRun;
        }

        public long getNextRun() {
            return nextRun;
        }

        public String getId() {
            return id;
        }

        public void print() {
            DateTime currentRunDateTime = new DateTime(currentRun);
            DateTime nextRunDateTime = new DateTime(nextRun);
            int currentRunMs = currentRunDateTime.getSecondOfMinute() * 1000 + currentRunDateTime.getMillisOfSecond();
            int nextRunMs = nextRunDateTime.getSecondOfMinute() * 1000 + nextRunDateTime.getMillisOfSecond();
            System.out.println(String.format("%d \t %d \t %d \t %s", currentRunMs,
                    nextRunMs, number, id));
        }
    }
}
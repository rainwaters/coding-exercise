package gamesys;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static java.nio.file.Paths.get;

/**
 * Created by Julia Fernee on 23/06/2015.
 */
public class FileReadTest {

    @Test
    public void readFileJava8() throws URISyntaxException, IOException {
        List<String> list = new LinkedList<>();
        URI path = getClass().getResource("/players.txt").toURI();
        Path p = get(path);
        Stream<String> filteredLines = Files.lines(p);
        filteredLines.forEach(l -> list.add(l));

        Assert.assertEquals("Did not read correctly from file", 2, list.size());

    }


}

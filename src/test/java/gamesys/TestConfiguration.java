package gamesys;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import gamesys.console.ConsoleGame;
import gamesys.service.bet.AsyncBetProcessor;
import gamesys.service.numgen.RandomNumberGenerator;
import gamesys.service.numgen.RandomNumberGeneratorService;
import gamesys.service.numgen.ScheduledRandomNumberGenerator;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.mockito.Mockito.spy;

/**
 * Created by JuliaFernee on 22/06/2015.
 */
@Configuration
public class TestConfiguration {

    private Logger log = Logger.getLogger(TestConfiguration.class);

    private ExecutorService executorService;
    private CountDownLatch countDownLatch;
    private EventBus eventBus;
    private RandomNumberGeneratorService randomNumberGeneratorService;
    private AsyncBetProcessor asyncBetProcessor;
    private Game consoleGame;

    private long numberGeneratorRateMs = 30000;
    private int numberGeneratorRangeFromInclusive = 1;
    private int numberGeneratorRangeToInclusive = 36;
    private long betRequestTimeoutMs = 20000;

    @PostConstruct
    public void init() {
        executorService = Executors.newCachedThreadPool();
        countDownLatch = new CountDownLatch(1);
        eventBus = new AsyncEventBus(executorService);
        createRandomNumberGeneratorService();
        asyncBetProcessor = new AsyncBetProcessor(eventBus, countDownLatch,
                betRequestTimeoutMs);
        eventBus.register(randomNumberGeneratorService);
        log.info("Event listerners are registered with event bus");
    }

    @Bean
    public EventBus eventBus() {
        log.info("GameEventBus is registered with spring context");
        return eventBus;
    }

    @Bean
    public AsyncBetProcessor asyncBetProcessor() {
        log.info("AsyncBetProcessor is registered with spring context");
        return spy(asyncBetProcessor);
    }

    @Bean
    public RandomNumberGeneratorService randomNumberGeneratorService() {
        log.info("RandomNumberGeneratorService is registered with spring context");
        return randomNumberGeneratorService;
    }

    @Bean
    public Game testGame() {
        consoleGame = new ConsoleGame();
        return spy(consoleGame);
    }

    private RandomNumberGenerator createRandomNumberGenerator() {
        return new ScheduledRandomNumberGenerator(
                numberGeneratorRateMs,
                numberGeneratorRangeFromInclusive,
                numberGeneratorRangeToInclusive);
    }

    private void createRandomNumberGeneratorService() {
        randomNumberGeneratorService = new RandomNumberGeneratorService();
        randomNumberGeneratorService.setCountDownLatch(countDownLatch);
        randomNumberGeneratorService.setRandomNumberGenerator(createRandomNumberGenerator());
        executorService.submit(randomNumberGeneratorService);
    }

    @PreDestroy
    public void destroy() {
        eventBus.unregister(randomNumberGeneratorService);
        log.info("Unregister listerners from event bus");
        executorService.shutdown();
        log.info("Sutdown thread pool");
    }

}

package gamesys;

import gamesys.service.bet.BetRequest;
import gamesys.service.bet.BetResponse;
import gamesys.service.bet.BetStrategy;
import gamesys.service.bet.OddEvenStrategy;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Julia Fernee on 21/06/2015.
 */
public class OddEvenStrategyTest {

    /**
     * Given bet request of bet amount 1.1 and bet on EVEN
     * when lucky number is even
     * than bet outcome is "WIN" and the winning amount is 1.1 x 2
     */
    @Test
    public void shouldFactorWinAmountForWinningBetOnEven() {
        int luckyNumber = 10;
        BetStrategy strategy = new OddEvenStrategy(OddEvenStrategy.BET.EVEN);
        BetRequest request = new BetRequest("Barbara", 1.1, strategy);
        BetResponse expectedBetResponse = new BetResponse(2.2, "WIN", luckyNumber);

        BetResponse actualResponse = strategy.play(request, luckyNumber);

        Assert.assertEquals("Incorrect bet response", expectedBetResponse, actualResponse);

    }

    /**
     * Given bet request of bet amount 3.0 and bet on ODD
     * when lucky number is odd
     * than bet outcome is "WIN" and the winning amount is 3.0 x 2
     */
    @Test
    public void shouldFactorWinAmountForWinningBetOnOdd() {
        int luckyNumber = 11;
        BetStrategy strategy = new OddEvenStrategy(OddEvenStrategy.BET.ODD);
        BetRequest request = new BetRequest("Barbara", 3, strategy);
        BetResponse expectedBetResponse = new BetResponse(6, "WIN", luckyNumber);

        BetResponse actualResponse = strategy.play(request, luckyNumber);

        Assert.assertEquals("Incorrect bet response", expectedBetResponse, actualResponse);

    }

    /**
     * Given bet request of bet amount 1.1 and bet on EVEN
     * when lucky number is odd
     * than bet outcome is "LOOSE" and the winning amount is 0
     */
    @Test
    public void shouldZeroWinAmountForLoosingBetOnEven() {
        int luckyNumber = 11;
        BetStrategy strategy = new OddEvenStrategy(OddEvenStrategy.BET.EVEN);
        BetRequest request = new BetRequest("Barbara", 1.1, strategy);
        BetResponse expectedBetResponse = new BetResponse(0, "LOOSE", luckyNumber);

        BetResponse actualResponse = strategy.play(request, luckyNumber);

        Assert.assertEquals("Incorrect bet response", expectedBetResponse, actualResponse);

    }

    /**
     * Given bet request of bet amount 3.0 and bet on ODD
     * when lucky number is even
     * than bet outcome is "LOOSE" and the winning amount is 0
     */
    @Test
    public void shouldZeroWinAmountForLoosingBetOnOdd() {
        int luckyNumber = 10;
        BetStrategy strategy = new OddEvenStrategy(OddEvenStrategy.BET.ODD);
        BetRequest request = new BetRequest("Barbara", 3, strategy);
        BetResponse expectedBetResponse = new BetResponse(0, "LOOSE", luckyNumber);

        BetResponse actualResponse = strategy.play(request, luckyNumber);

        Assert.assertEquals("Incorrect bet response", expectedBetResponse, actualResponse);

    }

}

package gamesys;

import gamesys.service.numgen.ScheduledRandomNumberGenerator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
public class ScheduledRandomNumberGeneratorTest {

    @Test
    public void shouldProduceRandomNumbersWithinRange() {
        long interval = 10L;
        long duration = 1000L;
        ScheduledRandomNumberGenerator gen = new ScheduledRandomNumberGenerator(interval, 1, 2);
        gen.start();
        long stopTime = System.currentTimeMillis() + duration;
        while (System.currentTimeMillis() < stopTime) {
            int rand = gen.getRandomNumber();
            Assert.assertTrue("Random number is not within range", rand == 1 || rand == 2);
        }
    }


}

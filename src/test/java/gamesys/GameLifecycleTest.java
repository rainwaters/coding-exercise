package gamesys;

import gamesys.console.ConsoleGame;
import gamesys.console.ConsoleGameConfiguration;
import gamesys.io.InputReader;
import gamesys.io.OutputWriter;
import gamesys.service.bet.AsyncBetProcessor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.mockito.Mockito.*;

/**
 * Created by Julia Fernee on 22/06/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = GameLifecycleTest.GameTestContextConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class GameLifecycleTest {

    @Configuration
    static class GameTestContextConfiguration {
        @Bean
        public AsyncBetProcessor mockAsyncBetProcessor() {
            AsyncBetProcessor processor = mock(AsyncBetProcessor.class);
            return processor;
        }

        @Bean
        public Game consoleGame() {
            return new ConsoleGame();
        }
    }

    @Autowired
    @Qualifier("consoleGame")
    private Game consoleGame;

    private InputReader reader = mock(InputReader.class);
    private OutputWriter writer = mock(OutputWriter.class);


    private GameConfiguration conf;

    @Test
    public void contextInjected() {
        Assert.assertTrue(consoleGame != null);
    }

    @Before
    public void configureGame() {
        conf = new ConsoleGameConfiguration(reader, writer);
        consoleGame.start((conf));
    }

    @Test
    public void shouldConfigurePlayersListWithTwoPlaers() {
        Assert.assertEquals("", 2, conf.getPlayers().size());
    }

    @Test
    public void shouldMarkGameAsTerminated() {
        consoleGame.terminate();
        Assert.assertTrue("Did not set game terminated flag.", consoleGame.isTerminated());
    }

    @Test
    public void shouldSutdownReaderWhenGameEnds() {
        consoleGame.end(); //shutdown on reader invoked
        verify(reader, times(1)).shutdown();
    }
}
